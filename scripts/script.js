"use strict";

document.addEventListener("DOMContentLoaded", setup);


//Calls all the necessary functions/event listener when the site starts
function setup(){

    //EventListener for the submit button
    document.querySelector("#mainForm").addEventListener("submit", submit);

    //EventListener for the checkboxes
    document.querySelector(".clearBox").addEventListener("click", checkedAction);

    //EventListener for the clear all button
    document.querySelector("#clear").addEventListener("click", clearAll);

    //Global Array, holding information for each task
    document.taskList = [];
    //Global variable, used to give each task a unique id.
    document.taskNumber = 0;

    //Loads loadTODO function which loads any undeleted task from previous sessions
    loadToDo();
    
    
}

// puts all information from the form into variables and sends it to the createTask function
function submit(e){

    //If the function was called through an event then makes sure that it doesnt submit to the server
    if(e){
        e.preventDefault();
        }
    console.log("Submit activated");

    //vTaskNumber is a consistent identifier for a task object, and will become its id
    let vTaskNumber = document.taskNumber;
    const vTask = document.querySelector("#task").value;
    const vDescription = document.querySelector("#description").value;
    const vImportance = document.querySelector("#importance").value;
    const vCategory = document.querySelector("input[name='category']:checked").value;

    createTask(vTaskNumber,vTask,vDescription,vImportance,vCategory);

    
}

//Create task, taking in all the inputs from a task object, and calls the saveToLocal function
function createTask(vTaskNumber, vTask, vDescription, vImportance, vCategory){

    //Creates the block of code that will contain all the task information
    let newArticle = document.createElement("article");
    newArticle.setAttribute("class", "taskBox")
    newArticle.setAttribute("id", vTaskNumber);

    //creates a H2 element, which will contain the name of the task
    let taskTitle = document.createElement("h2");
    taskTitle.setAttribute("class", "taskName");
    taskTitle.textContent= vTask;

    //creates a checkbox for the task
    let newCheckbox = document.createElement("input");
    newCheckbox.setAttribute("type", "checkbox");
    newCheckbox.setAttribute("class", "clearBox");
    newCheckbox.setAttribute("name", "finished");

    
    //creates the label for the checkbox
    let newCheckboxLabel = document.createElement("label");
    newCheckboxLabel.setAttribute("for", "finished");
    newCheckboxLabel.setAttribute("class", "fin");
    newCheckboxLabel.textContent = "Finished Task";

    //creates stars depending on the important of the task
    let newImportance = document.createElement("p");
    newImportance.setAttribute("class", "importance");
    //0 stars 
    if(vImportance == 0){
        newImportance.textContent = " \u2606 \u2606 \u2606";
    }
    //1 stars
    else if(vImportance == 1){
        newImportance.textContent = " \u2605 \u2606 \u2606";
    }
    //2 stars
    else if(vImportance == 2){
        newImportance.textContent = " \u2605 \u2605 \u2606";
    }
    //3 stars
    else if(vImportance == 3){
        newImportance.textContent = " \u2605 \u2605 \u2605";
    }

    //Creates category icons as well as setting up classes specific to each category
    let newCategory = document.createElement("p");
    newCategory.setAttribute("class", "category");
    //school category
    if(vCategory == "school"){
        newCategory.textContent = "\u26EA";
        newCategory.setAttribute("class", "schoolCategory");
    }
    //work category
    else if(vCategory == "work"){
        newCategory.textContent = "\u26FA";
        newCategory.setAttribute("class", "workCategory");
    }
    //personal category
    else if(vCategory == "personal"){
        newCategory.textContent = "\u2615";
        newCategory.setAttribute("class", "personalCategory");
    }
    
    
    //creates a field for the description of the task
    let newDescription = document.createElement("pre");
    newDescription.setAttribute("class", "taskDescription");
    newDescription.textContent= vDescription;


    //puts in all the elements into an article, which will be put inside the div of the html to be displayed
    newArticle.appendChild(taskTitle);
    newArticle.appendChild(newCheckbox);
    newArticle.appendChild(newCheckboxLabel);
    newArticle.appendChild(newImportance);
    newArticle.appendChild(newCategory);
    newArticle.appendChild(newDescription);
    document.querySelector(".template").appendChild(newArticle);

    //increments taskNumber
    document.taskNumber++;
    
    //creates and logs out the object of the newly created task
    let taskObject = {taskNumber : vTaskNumber , task : vTask, description : vDescription, importance : vImportance, category : vCategory};
    console.log(taskObject);
    //adds an eventListener to the checkbox each time a task is created
    document.getElementById(vTaskNumber).children[1].addEventListener("click", checkedAction);
    document.taskList.push(taskObject);
    //calls the saveIntoLocal function, which saves into localStorage
    saveIntoLocal();
    
    
}

//saves the array into a JSON form, and saves it into the local storage
function saveIntoLocal(){
    console.log("Local Storage updated");
    localStorage.setItem("JSONTaskList",JSON.stringify(document.taskList));
}


//deletes the task when the user clicks the checkmark
function checkedAction(){

    //finds the id of the parent of the checkbox (the article element)
    let taskId = this.parentNode.id
    //removes the article associated with the checked box
    this.parentNode.remove();
    //removes the task from the array by finding the index associated with the task's ID...
    //...since taskNumber is the same as the element's id
    document.taskList.splice(document.taskList.findIndex(taskListarray => taskListarray.taskNumber == taskId), 1);
    saveIntoLocal();
   
}
//when the clear all button is pressed, clears all task on screen, in the array and in the local storage
function clearAll(){

    //makes sure that the array isn't already empty
    if(document.taskList.length > 0){ 

        for (let i = 0; i < document.taskList.length; i++){
            let task = document.getElementById(i);
            task.remove();
        }
    }

    //clears out everything
    localStorage.clear();
    document.taskList = [];
    //resets the task number to 0 so we can start back at 0;
    document.taskNumber = 0;
    console.log(document.taskList);


}

//loads saved tasks
function loadToDo() {

    //Converts any previous JSON into the array
    if (localStorage.getItem("JSONTaskList")){

        //creates a temporary global array identical to a previous array from another session
        document.tempArr = JSON.parse(localStorage.getItem("JSONTaskList"));

        //length of the array
        let arrayLength = document.tempArr.length;
        
        //retrieves information from each object inside the array, and sends to createTask
        for (let i = 0; i < arrayLength; i++) {
           let vTaskNumber = document.taskNumber; 
           let vTask = document.tempArr[i].task;
           let vDescription = document.tempArr[i].description;
           let vImportance = document.tempArr[i].importance;
           let vCategory = document.tempArr[i].category;

           //creates tasks with inputs from the array
           createTask(vTaskNumber,vTask,vDescription,vImportance,vCategory);
        }

    }
}


